- el icono de la app se guarda en la carpeta mipmap
- las demas imagenes como bakcground o alguna otra en drawable
- https://diegolaballos.com/blog/diferencias-entre-la-carpeta-drawable-y-mimap-en-android/


Cambiarle el icono

click en app(con la perspectiva de android)
new -> image asset

    // poner el icono en el action bar de la app

    onCreate(...){
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
    }
    
- para evitar el reinicio del activity cuando se redimensiona la pantalla - fijate el archivo manifest de este proyecto

- otro dato si no se quiere que sea responsive
````xml
<!--con esto le decimos que siempre sea vertical-->
<activity android:name=".MainActivity" android:screenOrientation="portrait" />

````