package com.android.primerproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.primerproyecto.model.AdminSQLiteOpenHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LevelFiveActivity extends AppCompatActivity {

    @BindView(R.id.tv_player) TextView tv_player;
    @BindView(R.id.iv_vida) ImageView iv_vidas;
    @BindView(R.id.et_response) EditText user_response;
    @BindView(R.id.tv_score) TextView score;
    @BindView(R.id.iv_uno) ImageView uno;
    @BindView(R.id.iv_dos) ImageView dos;
    @BindView(R.id.iv_operador) ImageView operador;

    MediaPlayer mp,mp_great,mp_bad;
    String player;
    String numeros [] = {"cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"};
    int score_int, numAleatorio_uno, numAleatorio_dos, resultadoOriginal, vidas = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_five);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        ButterKnife.bind(this);
        Toast.makeText(this, "Nivel 5 - Multiplicaciones", Toast.LENGTH_LONG).show();

        player = getIntent().getStringExtra("jugador");
        tv_player.setText("Jugador : "+player);

        vidas = Integer.parseInt(getIntent().getStringExtra("vidas"));

        score_int = Integer.parseInt(getIntent().getStringExtra("score"));
        score.setText("Score: "+ score_int);

        if(vidas == 3){
            iv_vidas.setImageResource(R.drawable.tresvidas);
        } if(vidas == 2){
            iv_vidas.setImageResource(R.drawable.dosvidas);
        } if(vidas == 1){
            iv_vidas.setImageResource(R.drawable.unavida);
        }

        mp = MediaPlayer.create(this, R.raw.goats);
        mp.start();
        mp.setLooping(true);

        mp_great = MediaPlayer.create(this, R.raw.wonderful);
        mp_bad = MediaPlayer.create(this, R.raw.bad);

        this.generateMathOperations();
    }

    protected void generateMathOperations() {
        if(score_int>=50){
            Intent intent = new Intent(this, LevelSixActivity.class);

            intent.putExtra("jugador", player);
            intent.putExtra("score", score_int+"");
            intent.putExtra("vidas", vidas+"");

            startActivity(intent);
            mp.stop();
            mp_great.stop();
            mp_bad.stop();
            mp_bad.release();
            mp_great.release();
            mp.release();
            finish();
            return;
        }

        numAleatorio_uno = (int)(Math.random() * (9 - 0 + 1) + 0);
        numAleatorio_dos = (int)(Math.random() * (9 - 0 + 1) + 0);
        operador.setImageResource(R.drawable.multiplicacion);
        resultadoOriginal =  numAleatorio_uno * numAleatorio_dos;
        if(resultadoOriginal>=82){
            generateMathOperations();
            return;
        }

        int idImgUno = getResources().getIdentifier(numeros[numAleatorio_uno], "drawable", getPackageName());
        int idImgDos = getResources().getIdentifier(numeros[numAleatorio_dos], "drawable", getPackageName());
        uno.setImageResource(idImgUno);
        dos.setImageResource(idImgDos);
    }

    @OnClick({R.id.btn_action})
    protected void checked_result(){
        String res = user_response.getText().toString().trim();

        if(res.isEmpty()){
            Toast.makeText(this,"Escribe tu respuesta", Toast.LENGTH_SHORT).show();
            user_response.requestFocus();
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.showSoftInput(user_response, InputMethodManager.SHOW_IMPLICIT);
            return;
        }
        int respuesta_jugador = Integer.parseInt(res);

        if(respuesta_jugador == resultadoOriginal){
            mp_great.start();
            score.setText("Score: " + (++score_int));
            updateDB();

        }else{
            mp_bad.start();
            updateDB();
            vidas--;
            switch (vidas){
                case 3:
                    iv_vidas.setImageResource(R.drawable.tresvidas);
                    break;
                case 2:
                    Toast.makeText(this, "Te quedan 2 vidas!", Toast.LENGTH_SHORT).show();
                    iv_vidas.setImageResource(R.drawable.dosvidas);
                    break;
                case 1:
                    Toast.makeText(this, "Te queda 1 vida!", Toast.LENGTH_SHORT).show();
                    iv_vidas.setImageResource(R.drawable.unavida);
                    break;
                case 0:
                    Toast.makeText(this, "Has perdido todas tus vidas!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    mp.stop();
                    mp.release();
                    finish();
                    return;
            }
        }
        user_response.setText("");
        generateMathOperations();
    }
    protected void updateDB(){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,"bd_frutiapp",null,1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        Cursor consulta = bd.rawQuery("SELECT * FROM puntajes WHERE score = (SELECT MAX(score) FROM puntajes)", null);

        if(consulta.moveToFirst()){
            String temp_nombre = consulta.getString(0);
            String temp_score = consulta.getString(1);

            int bestScore = Integer.parseInt(temp_score);

            if(score_int > bestScore){
                ContentValues modificacion = new ContentValues();
                modificacion.put("nombre", player);
                modificacion.put("score", score_int);

                bd.update("puntajes", modificacion, "score=" + bestScore, null);
            }
        }else{
            ContentValues insertar = new ContentValues();

            insertar.put("nombre", player);
            insertar.put("score", score_int);

            bd.insert("puntajes", null, insertar);
        }
        bd.close();
    }
}