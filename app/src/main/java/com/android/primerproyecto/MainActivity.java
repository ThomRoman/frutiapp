package com.android.primerproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.primerproyecto.model.AdminSQLiteOpenHelper;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private ImageView iv_imgLogo;
    private EditText et_name;
    private TextView record;
    private MediaPlayer mp;

    private int number_random = (int)(Math.random() * (10 - 0 + 1) + 0); // del 0 al 10 tomando los extremos
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        iv_imgLogo = findViewById(R.id.iv_logo);
        et_name = findViewById(R.id.et_name);
        record = findViewById(R.id.tv_record);

        // portada aleatoria

        int idImageBackground;
        if(number_random == 0 || number_random == 10){
            idImageBackground = getResources().getIdentifier("mango", "drawable", getPackageName());
            iv_imgLogo.setImageResource(idImageBackground);
        }else if(number_random == 1 || number_random ==9){
            idImageBackground = getResources().getIdentifier("manzana", "drawable", getPackageName());
            iv_imgLogo.setImageResource(idImageBackground);
        }else if(number_random == 2 || number_random == 8){
            idImageBackground = getResources().getIdentifier("fresa", "drawable", getPackageName());
            iv_imgLogo.setImageResource(idImageBackground);
        }else if(number_random == 3 || number_random == 7){
            idImageBackground = getResources().getIdentifier("naranja", "drawable", getPackageName());
            iv_imgLogo.setImageResource(idImageBackground);
        }else if(number_random == 4 || number_random ==6 ){
            idImageBackground = getResources().getIdentifier("sandia", "drawable", getPackageName());
            iv_imgLogo.setImageResource(idImageBackground);
        }else{
            idImageBackground = getResources().getIdentifier("uva", "drawable", getPackageName());
            iv_imgLogo.setImageResource(idImageBackground);
        }



        // traerme lo que esta en la bd para actualizar el record
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,"bd_frutiapp",null,1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor consulta = bd.rawQuery(
                "SELECT * FROM puntajes WHERE score = (SELECT MAX(score) FROM puntajes)",
                null
        );

        if(consulta.moveToFirst()){
            String temp_nombre = consulta.getString(0);
            String temp_score = consulta.getString(1);
            record.setText("Record: " + temp_score + " de " + temp_nombre);
        }
        bd.close();
        mp = MediaPlayer.create(this, R.raw.alphabet_song);
        mp.start();
        mp.setLooping(true);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_login})
    protected void play(){
        String name = et_name.getText().toString().trim();
        if(name.isEmpty()){
            Toast.makeText(this, "Debes escribir tu nombre", Toast.LENGTH_LONG).show();
            et_name.requestFocus();
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et_name, InputMethodManager.SHOW_IMPLICIT);
            return;
        }
//        Toast.makeText(this, "Correcto", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LevelOneActivity.class);
        intent.putExtra("player", name);
        startActivity(intent);

        mp.stop(); // detener la musica
        mp.release(); // liberar recursos
        finish(); // terminar activity

    }
}